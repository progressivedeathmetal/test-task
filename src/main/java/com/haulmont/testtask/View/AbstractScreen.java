package com.haulmont.testtask.View;

import com.haulmont.testtask.View.toolbars.LookupTool;
import com.haulmont.testtask.View.toolbars.OkCancelBar;
import com.haulmont.testtask.View.toolbars.OkCancelBarListener;
import com.haulmont.testtask.View.toolbars.ToolBarListener;
import com.vaadin.data.Validator;
import com.vaadin.event.FieldEvents;
import com.vaadin.ui.*;
import org.vaadin.viritin.fields.MTable;

import static com.haulmont.testtask.View.AbstractScreen.defineTable;

/**
 * Created by Inferno on 07.01.2017.
 */
public abstract class AbstractScreen extends CustomComponent implements ToolBarListener {
    abstract void updateTableData(Table table);

    TableContainer tableContainer = new TableContainer();


    public TableContainer getTableContainer() {
        return tableContainer;
    }

    public void openLookup(LookupTool lookupTool) {
        LookupWindow window = new LookupWindow(lookupTool);
        UI.getCurrent().addWindow(window);
        this.updateTableData(window.getContainer().getTable());
    }


    static MTable defineTable() {
        MTable table = new MTable()
                .withFullWidth();
        table.setSelectable(true);
        table.setImmediate(true);
        return table;
    }

    void validateRealtime(AbstractTextField field, FieldEvents.TextChangeEvent event) {
        try {
            field.setValue(event.getText());
            field.setCursorPosition(event.getCursorPosition());
            field.validate();
        } catch (Validator.InvalidValueException e) {
        } //сообщение выводится иначе
    }


    class LookupWindow extends ConfiguredWindow implements OkCancelBarListener {
        LookupTool lookupTool;

        LookupWindow(LookupTool alookupTool) {
            setCaption("Произведите выбор");
            setContent(initContent());
            setSizeFull();
            this.lookupTool = alookupTool;
        }


        TableContainer getContainer() {
            return container;
        }

        private TableContainer container = new TableContainer();

        private Component initContent() {
            VerticalLayout layout = new VerticalLayout();
            layout.addComponent(container);
            layout.addComponent(new OkCancelBar(this));
            layout.setMargin(true);
            return layout;

        }


        @Override
        public void okClick() {

            lookupTool.setConvertedValue(container.getSelectedItem(lookupTool.getType()));
            if (lookupTool.getConvertedValue() == null) {
                nothingToSelect();
            } else {
                close();
            }
        }

        @Override
        public void cancelClick() {
            close();
        }
    }

    void nothingToSelect() {
        Notification.show("Ничего не выбрано", Notification.Type.WARNING_MESSAGE);
    }
}

class TableContainer extends CustomComponent {

    private Table table = defineTable();
    private VerticalLayout layout = new VerticalLayout();

    TableContainer() {
        layout.addComponent(table);
        setCompositionRoot(layout);
    }

    <T> T getSelectedItem(Class<T> tClass) {
        Object value = getTable().getValue();
        return (T) getTable().getValue();
    }

    Table getTable() {
        return table;
    }

}

