package com.haulmont.testtask.DAO;

import com.haulmont.testtask.DAO.entity.PersistObject;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by Inferno on 29.12.2016.
 */
abstract class Dao {
    private EntityManagerFactory getEntityManagerFactory() {
        return entityManagerFactory;
    }

    private static EntityManager entityManager;

    private static EntityManagerFactory entityManagerFactory = Persistence
            .createEntityManagerFactory("com.haulmont.testtask");

    protected EntityManager getEntityManager() {
        entityManager = entityManager == null ? getEntityManagerFactory().createEntityManager() : entityManager;
        return entityManager;
    }

    public void saveOrUpdate(PersistObject entity) {
        getEntityManager().getTransaction().begin();
        if (entity.getId() == null) {
            getEntityManager().persist(entity);
        } else {
            getEntityManager().merge(entity);
        }
        getEntityManager().getTransaction().commit();
    }

    public void delete(PersistObject entity) {
        getEntityManager().getTransaction().begin();
        getEntityManager().remove(getEntityManager().find(entity.getClass(), entity.getId()));
        getEntityManager().getTransaction().commit();
    }


}
